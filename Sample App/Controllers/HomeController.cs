﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Newtonsoft.Json.Linq;



namespace Sample_App.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SQL()
        {
            ViewBag.Message = "Let's take a look at how we connect to a SQL Server instance in HDP.";

            return View();
        }

        public ActionResult Rabbit()
        {
            ViewBag.Message = "Let's see how to interact with a RabbitMQ queue that is bound to this app in HDP.";
            //Logic should be removed from view controllers into seperate logic folder/class.
            //Keeping here for demo purposes
            ConnectionFactory factory = new ConnectionFactory();
            string vcap = Environment.GetEnvironmentVariable("VCAP_SERVICES");
            if (vcap != null)
            {
                JObject services = JObject.Parse(vcap);
                string user = services["rabbitmq3-3.1"].First["credentials"]["username"].ToString();
                string vhost = services["rabbitmq3-3.1"].First["credentials"]["vhost"].ToString();
                string hostName = services["rabbitmq3-3.1"].First["credentials"]["hostname"].ToString();
                string password = services["rabbitmq3-3.1"].First["credentials"]["password"].ToString();
                string uri = services["rabbitmq3-3.1"].First["credentials"]["uri"].ToString();
                factory.UserName = user;
                factory.Password = password;
                factory.VirtualHost = vhost;
                factory.HostName = hostName;
                factory.Uri = uri;
               

                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare("hello", false, false, false, null);

                        DateTime now = DateTime.Now;
                        string message = "Hello World! at "+now.ToString();
                        var body = Encoding.UTF8.GetBytes(message);

                        channel.BasicPublish("", "hello", null, body);

                        //For demo purposes only- this writes received messages to logstream
                        //You would uncomment the while loop and put in the receiver component
                        var consumer = new QueueingBasicConsumer(channel);
                        channel.BasicConsume("hello", true, consumer);

    
                        Console.WriteLine(" [*] Waiting for messages." +
                                                 "To exit press CTRL+C");
                       // while (true)
                        //{
                        var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();

                        var body_rec = ea.Body;
                        var message_rec = Encoding.UTF8.GetString(body_rec);
                        Console.WriteLine(" [x] Received {0}", message_rec);
                        //}
                    }
                }

            }


            return View();
        }

    }
}